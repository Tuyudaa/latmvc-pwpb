<?php 

class Home extends Controller{
    public function index()
    {   
        $data['title'] = 'Home';
        $data['nama'] = $this->model('HomeModel')->getUser();
        $data['name'] = $this->model('UserModel')->getAllUser();

        $this->view('templates/header', $data);
        $this->view('home/index', $data);
        $this->view('templates/footer', $data);
    }

    public function about($company = ' ' )
    {   
        $data['title'] = 'Home';
        $data['nama'] = $this->model('HomeModel')->getUser();
        $data['name'] = $this->model('UserModel')->getAllUser();
        $data["judul"]="About";
        $data["company"]= $company;
        $this->view('templates/header', $data);
        $this->view('home/index', $data);
        $this->view('templates/footer', $data);
    }

    public function show($id){
        $data['user'] = $this->model('UserModel')->getAllUser;
        $data['user'] = $this->model('UserModel')->getUserById($id);

        $this->view('templates/header', $data);
        $this->view('home/index', $data);
        $this->view('templates/footer', $data);
    }

    public function create(){
        if($this->model('UserModel')->createUser($_POST) > 0){
            header('Location: ' . BASE_URL . '/home/about');
        
        } 
        else{
            echo("ERROR PUAKKKK");
        }
    }
}
?>