<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>Signin Template · Bootstrap v5.0</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/sign-in/">

    

    <!-- Bootstrap core CSS -->
    <link href="http://localhost/latmvc/public/assets/CSS/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="<?= BASE_URL ?>/assets/CSS/signin.css" rel="stylesheet">
  </head>
  <body class="text-center">
  
<main class="form-signin m-auto border">
  <form method="post" action="<?=BASE_URL?>/auth/registerPost">
    <img class="mb-4" src="/laragon/www/latmvc/app/view/register/bootstrap-logo.svg" alt="" width="72" height="57">
    <h1 class="h3 mb-3 fw-normal">Please sign in</h1>

    <div class="form-floating">
      <input type="username" class="form-control" id="username" name="username" placeholder="username">
      <label for="username">Username</label>
    </div>
    <div class="form-floating">
      <input type="first_name" class="form-control" id="first_name" name="first_name" placeholder="first_name">
      <label for="first_name">First Name</label>
    </div>
    <div class="form-floating">
      <input type="last_name" class="form-control" id="last_name" name="last_name" placeholder="last_name">
      <label for="floatingPassword">Last Name</label>
    </div>
    <div class="form-floating">
      <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com">
      <label for="email">Email</label>
    </div>
    <div class="form-floating">
      <input type="password" class="form-control" id="Password" name="password" placeholder="Password">
      <label for="Password">Password</label>
    </div>
    <div class="checkbox mb-3">
      <label>
        <input type="checkbox" value="remember-me"> Remember me
      </label>
    </div>
    <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
    <p class="mt-5 mb-3 text-muted">&copy; 2017–2021</p>
  </form>
</main>


    
  </body>
</html>
