<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Register</title>
    <link href="<?= BASE_URL ?>/assets/CSS/bootstrap.min.css" rel="stylesheet">
    <link href="<?= BASE_URL ?>/assets/CSS/dashboard.css" rel="stylesheet">
    
</head>
<body>
<div class="card m-auto shadow mt-5" style="width: 30%;">
        <main class="form-control">
             <!-- view template -->
            <div class="row">
                <div class="col-lg-6">
                <?php Flasher::flash();?>
                </div>
            </div>
            <div class="text-center mt-2">
                <img src="<?= BASE_URL ?>/assets/image/bootstrap-logo-shadow.png" alt="" width="100">
            </div>
            <form method="post" action="<?= BASE_URL ?>/auth/loginpost">
                <h3 class="text-center my-3">Please Sign Up</h3>
                <?php if(isset($data['msg'])) {?>
                    <div><?=$data['msg']?></div>
                <?php } ?>
                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingInput" placeholder="email" name="email" required/>
                    <label for="floatingInput">Email</label>
                </div>
                <div class="form-floating">
                    <input type="password" class="form-control" id="floatingInput" placeholder="password" name="password" required/>
                    <label for="floatingInput">Password</label>
                </div>
                <div class="mt-2">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div>
                <button class="w-100 mt-2 btn btn-lg btn-primary mb-3" type="submit" name="submit">Sign In</button>
                <div class="mb-3 mt-2 w-100 text-center">
                    <a href="<?= BASE_URL ?>/auth/login">Lupa Password</a>
                </div>
                <div class="mb-3 w-100 text-center">
                    <p class="d-inline">have a account? </p><a href="<?= BASE_URL ?>/auth/register">Register</a>
                </div>
            </form>
        </main>
    </div>
</body>
</html>
